package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class P01GradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(P01GradleApplication.class, args);
	}

}
